import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class databaseSupport {

    static String dbName = "reggie";
    static String connectionURL = "jdbc:derby:" + dbName + ";create=true";

    //Create function for course class
    public static Object Create(String className, Object courseCredits,String strings,Course course) throws Exception {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();

            switch(className){
                case "Course":
                    int cd = (Integer) courseCredits;
                    statement.executeUpdate("DELETE FROM course WHERE name = '" + strings + "'");
                    statement.executeUpdate("INSERT INTO course VALUES ('" + strings + "', " + cd + ")");
                    return new Course(strings, cd);
                case "Schedule":
                    statement.executeUpdate("DELETE FROM schedule WHERE name = '" + strings + "'");
                    return new Schedule(strings);
                case "Offering":
                    ResultSet result = statement.executeQuery("SELECT MAX(id) FROM offering");
                    result.next();
                    int newId = 1 + result.getInt(1);

                    statement.executeUpdate("INSERT INTO offering VALUES (" + newId + ",'"
                            + course.getName() + "','" + strings + "')");
                    return new Offering(newId, course, strings);
            }
        } finally {
            try {
                conn.close();
            } catch (Exception ignored) {
            }
        }
        return null;
    }

    //Find function for all class
    public static Object Find(String studentname, String classname, Object id){
        Connection conn = null;
        try{
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();
            switch(classname){
                case "Course":
                    ResultSet result1 = statement.executeQuery("SELECT * FROM course WHERE name = '" + studentname
                            + "'");
                    if (!result1.next())
                        return null;
                    return new Course(studentname, result1.getInt("Credits"));
                case "Schedule":
                    ResultSet result2 = statement.executeQuery("SELECT * FROM schedule WHERE name= '" + studentname
                            + "'");

                    Schedule schedule = new Schedule(studentname);

                    while (result2.next()) {
                        int offeringId = result2.getInt("offeringId");
                        Offering offering = Offering.find(offeringId);
                        schedule.add(offering);
                    }
                    return schedule;
                case "Offering":
                    int id2 = (Integer) id;
                    ResultSet result3 = statement.executeQuery("SELECT * FROM offering WHERE id =" + id2
                            + "");
                    if (result3.next() == false)
                        return null;
                    String courseName = result3.getString("name");
                    Course course = Course.find(courseName);
                    String dateTime = result3.getString("daysTimes");
                    conn.close();
                    return new Offering(id2, course, dateTime);
            }
        } catch(Exception ex){
            return null;
        }finally{
            try{
                conn.close();
            }catch(Exception ignored){

            }
        }
        return null;
    }

    //Update function for all classes
    public static void Update(String className,int credits, String strings, Course course, ArrayList<Offering> schedule) throws Exception {

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();


                if(className== "Schedule") {
                    //int int3 = (Integer) ints;
                    statement.executeUpdate("DELETE FROM schedule WHERE name = '" + strings + "'");
                    for (int i = 0; i < schedule.size(); i++) {
                        Offering offering = (Offering) schedule.get(i);
                        int resultCount = statement.executeUpdate("INSERT INTO schedule VALUES('" + strings + "',"
                                + offering.getId() + ")");
                    }
                }
                else if(className=="Offering") {
                    //int int2 = (Integer) ints;
                    statement.executeUpdate("DELETE FROM offering WHERE id=" + credits + "");
                    statement.executeUpdate("INSERT INTO offering VALUES(" + credits + ",'" + course.getName()
                            + "','" + strings + "')");
                }
                else if(className=="Course") {
                    statement.executeUpdate("DELETE FROM course WHERE name = '" + strings + "'");
                    statement.executeUpdate("INSERT INTO course VALUES('" + strings + "'," + credits + ")");
                }
        }
        finally {
            try {
                conn.close();
            } catch (Exception ignored) {
            }
        }
    }


    //refactoring deleteAll function from Schedule class
    public static void deleteAll() throws Exception {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();

            statement.executeUpdate("DELETE FROM schedule");
        } finally {
            try {
                conn.close();
            } catch (Exception ignored) {
            }
        }
    }


    //refactoring all function from Schedule class
    public static Collection<Schedule> all() throws Exception {
        ArrayList<Schedule> result = new ArrayList<Schedule>();
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();
            ResultSet results = statement.executeQuery("SELECT DISTINCT name FROM schedule ORDER BY name");

            while (results.next())
                result.add(Schedule.find(results.getString("name")));
        } finally {
            try {
                conn.close();
            } catch (Exception ignored) {
            }
        }

        return result;
    }


}

