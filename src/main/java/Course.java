import java.sql.*;

public class Course {
	private String name;
	private int credits;

	public static Course create(String name, int credits) throws Exception {
		return (Course) databaseSupport.Create("Course",credits,name,null);
	}

	public static Course find(String name) {
		return (Course) databaseSupport.Find(name,"Course",null);
	}


	public void update() throws Exception {
		databaseSupport.Update("Course",credits,name,null,null );
	}

	Course(String name, int credits) {
		this.name = name;
		this.credits = credits;
	}

	public int getCredits() {
		return credits;
	}

	public String getName() {
		return name;
	}
}
