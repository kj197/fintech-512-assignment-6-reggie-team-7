import java.sql.*;

public class Offering {
	private int id;
	private Course course;
	private String daysTimes;

	public static Offering create(Course course, String daysTimesCsv) throws Exception {
		return (Offering) databaseSupport.Create("Offering",null,daysTimesCsv,course);
	}

	public static Offering find(int id) {
		return (Offering) databaseSupport.Find(null,"Offering",id);
	}



	public void update() throws Exception {
		databaseSupport.Update("Offering",id,daysTimes,course,null);
	}


	public Offering(int id, Course course, String daysTimesCsv) {
		this.id = id;
		this.course = course;
		this.daysTimes = daysTimesCsv;
	}

	public int getId() {
		return id;
	}

	public Course getCourse() {
		return course;
	}

	public String getDaysTimes() {
		return daysTimes;
	}

	public String toString() {
		return "Offering " + getId() + ": " + getCourse() + " meeting " + getDaysTimes();
	}
}
