# Assignment 6 Database - Reggie

Team member: Kangjie Ji(kj197), Kuai Yu(ky108), Yifei Dong(yd158)

Course registration project 

The code has been updated to run under JDK 17 and JDBC 4.3.  Rather than using the ODBC bridge, this version uses Apache Derby.

Questions:
Refactoring deals mostly with code smells. But there are data smells too; the database community has notions of what constitutes a good data design.
1) What potential problems do you see in this database structure?
2) What changes to the database might address them?
3) Identify smells, especially the duplication (and the differences!) between these classes within this repository
4) What is the pattern of access for persistence? (That is, trace the life of an object from creation to destruction.)
5) The database-related routines are tantalizingly similar. Apply refactorings that will reduce this duplication.  Apply other refactorings as opportunities arise.
6) Create a class that provides the bulk of the database support. 
7) Should the "database support" class be a parent of these objects or a separate class?

Questions 1-4 and 7 do not require any code changes.
